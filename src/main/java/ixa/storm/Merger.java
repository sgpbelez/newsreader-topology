package ixa.storm;

import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Fields;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;


public class Merger extends BaseRichBolt
{
    private String name;
    private OutputCollector collector;
    private LogManager logManager;
    private TopologyConf appConfig;
    private Integer numSources;
    private Map<String, List<Tuple>> srcsReadyForDoc;
    private MongoNaf mongo;
    private Integer workerId;
    private static final long serialVersionUID = 42L; // Serializable... 

    
    public Merger(String name, TopologyConf config, Integer numSources)
    {
	this.name = name;
	this.numSources = numSources;
	this.appConfig = config;
	this.srcsReadyForDoc = new HashMap<String, List<Tuple>>();
    }

    public void prepare(Map conf, TopologyContext context, OutputCollector collector)
    {
	this.collector = collector;
	this.workerId = context.getThisWorkerPort();
	// Mongo
	try {
	    if (this.appConfig.useMongoDB()) {
		String mongoHost = this.appConfig.getMongoDBHost();
		String mongoPort = this.appConfig.getMongoDBPort();
		String mongoDb = this.appConfig.getMongoDBName();
		this.mongo = MongoNaf.instance(mongoHost, Integer.parseInt(mongoPort), mongoDb);
	    }
	} catch(Exception e) {
	    e.printStackTrace();
	}
	this.logManager = new LogManager(this.appConfig.logStdOutput(), this.appConfig.logMongoDB(), this.workerId);
	if (this.appConfig.logMongoDB()) {
	    this.logManager.setMongoManager(this.mongo);
	}
    }

    public void execute(Tuple tuple)
    {
	String docId = tuple.getStringByField("doc_id");

	List<Tuple> srcTuples = this.srcsReadyForDoc.get(docId);
	if (srcTuples == null) {
	    srcTuples = new ArrayList<Tuple>();
	    this.srcsReadyForDoc.put(docId, srcTuples);
	}
	srcTuples.add(tuple);
	if (srcTuples.size() < this.numSources) {
	    return;
	}

	this.collector.emit(srcTuples, tuple.getValues());
	for (Tuple srcTuple : srcTuples) {
	    this.collector.ack(srcTuple);
	}
	this.srcsReadyForDoc.remove(docId);
    }


    public void declareOutputFields(OutputFieldsDeclarer declarer)
    {
	if (this.appConfig.useMongoDB()) {
	    declarer.declare(new Fields("doc_id", "granularity_info"));
	} else {
	    declarer.declare(new Fields("doc_id", "granularity_info", "naf"));
	}
    }

}