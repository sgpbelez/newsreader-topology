package ixa.storm;

import ixa.storm.NLPSpout.InputTuple;


public interface IInputManager
{
    void prepare(TopologyConf config);

    InputTuple getNextDocument();

    void init(String docId);

    void ack(String docId);

    void fail(String docId);
}
