package ixa.storm;

import backtype.storm.generated.StormTopology;
import backtype.storm.topology.TopologyBuilder;
import backtype.storm.topology.BoltDeclarer;
import backtype.storm.tuple.Fields;
import java.util.Map;
import java.util.HashMap;
import java.util.List;
import java.util.ArrayList;
import java.util.Iterator;


public class Topology
{
    private String name;
    private Boolean batch;
    private List<InputModule> inputModules;
    private List<Module> modules;
    private List<OutputModule> outputModules;


    public Topology(String name, Boolean batch)
    {
	this.name = name;
	this.batch = batch;
	this.inputModules = new ArrayList<InputModule>();
	this.modules = new ArrayList<Module>();
	this.outputModules = new ArrayList<OutputModule>();
    }

    public String getName()
    {
	return this.name;
    }

    public void setName(String name)
    {
	this.name = name;
    }

    public List<String> getInputNames()
    {
	List<String> inputs = new ArrayList<String>();
	for (InputModule module : this.inputModules) {
	    inputs.add(module.name);
	}
	return inputs;
    }

    public void setInput(String name, IInputManager inputManager, int parallelismHint)
    {
	this.inputModules.add(new InputModule(name, inputManager, parallelismHint));
    }

    public void setModule(String name, INLPModule instance, int parallelismHint, List<String> inLayers, List<String> outLayers, GranularityType granularity, List<String> sourceModules, String vmType)
    {
	this.modules.add(new Module(name, instance, sourceModules, inLayers, outLayers, granularity, parallelismHint, vmType));
    }

    public void setModule(String name, INLPModule instance, int parallelismHint, List<String> inLayers, List<String> outLayers, GranularityType granularity, String sourceModule, String vmType)
    {
	List<String> sourceModules = new ArrayList<String>();
	sourceModules.add(sourceModule);
	this.setModule(name, instance, parallelismHint, inLayers, outLayers, granularity, sourceModules, vmType);
    }

    public void setOutput(String name, IOutputManager outputManager, int parallelismHint)
    {
	this.outputModules.add(new OutputModule(name, outputManager, parallelismHint));
    }

    StormTopology getStormTopology(TopologyConf config, Integer numWorkers)
    {
	TopologyBuilder builder = new TopologyBuilder();
	Map<String, GranularityType> moduleGrns = new HashMap<String, GranularityType>();

	Iterator<InputModule> inputIt = this.inputModules.iterator();
	while (inputIt.hasNext()) {
	    InputModule inputModule = inputIt.next();
	    NLPSpout spout = new NLPSpout(inputModule.name, inputModule.instance, config);
	    System.out.println("> SPOUT: " + inputModule.name + " [" + inputModule.parallelismHint + "]");
	    builder.setSpout(inputModule.name, spout, inputModule.parallelismHint);
	    moduleGrns.put(inputModule.name, GranularityType.DOCUMENT);
	}

	Integer splitterCounter = 0;
	Integer joinerCounter = 0;
	boolean lastBolt = false;
	String lastNLPBoltName = null;
	Iterator<Module> moduleIt = this.modules.iterator();
	while (moduleIt.hasNext()) {
	    Module module = moduleIt.next();
	    List<String> sourceModules = new ArrayList<String>(module.sourceModules);
	    
	    if (!moduleIt.hasNext()) { // It is the last bolt
		lastNLPBoltName = module.name;
		if (this.outputModules.size() == 0) {
		    lastBolt = true;
		}
	    }
	    
	    /* Granularity (splitters and joiners) */
	    for (int i = 0; i < sourceModules.size(); i++) {
		String sourceName = sourceModules.get(i);
		GranularityType sourceGrn = moduleGrns.get(sourceName);
		if (module.granularity.compareTo(sourceGrn) < 0) {
		    String boltId = "splitter" + (++splitterCounter);
		    Splitter bolt = new Splitter(boltId, config, module.granularity);
		    System.out.println(">>> SPLITTER: "+boltId+" [1] ("+sourceName+")");
		    builder.setBolt(boltId, bolt, 1).shuffleGrouping(sourceName);
		    //builder.setBolt(boltId, bolt, 1).fieldsGrouping(sourceName, new Fields("doc_id"));
		    sourceModules.set(i, boltId);
		} else if (module.granularity.compareTo(sourceGrn) > 0) {
		    String boltId = "joiner" + (++joinerCounter);	
		    Joiner bolt = new Joiner(boltId, config, module.granularity);
		    System.out.println(">>> JOINER: "+boltId+" [1] ("+sourceName+")");
		    //builder.setBolt(boltId, bolt, 1).shuffleGrouping(sourceName);
		    builder.setBolt(boltId, bolt, 1).fieldsGrouping(sourceName, new Fields("doc_id"));
		    sourceModules.set(i, boltId);
		}
	    }

	    /* Mergers */
	    if (sourceModules.size() > 1) {
		String mergerName = "merger_" + module.name;
		Merger mergerBolt = new Merger(mergerName, config, sourceModules.size());
		BoltDeclarer mergerBoltDecl = builder.setBolt(mergerName, mergerBolt, 1);
		Iterator<String> sourceIt = sourceModules.iterator();
		System.out.println(mergerName + " [1]");
		while (sourceIt.hasNext()) {
		    String sourceName = sourceIt.next();
		    System.out.println("\t< " + sourceName);
		    if (this.batch) {
			mergerBoltDecl.localOrShuffleGrouping(sourceName);
		    } else {
			mergerBoltDecl.fieldsGrouping(sourceName, new Fields("doc_id"));
		    }
		}
		sourceModules.clear();
		sourceModules.add(mergerName);
	    }

	    NLPBolt bolt = new NLPBolt(module.name, module.instance, module.inLayers, module.outLayers, config, lastBolt);
	    BoltDeclarer boltDecl = builder.setBolt(module.name, bolt, module.parallelismHint).setNumTasks(numWorkers);
	    if (module.name != null) {
		boltDecl.addConfiguration("vm_type", module.vmType);
	    }
	    Iterator<String> sourceIt = sourceModules.iterator();
	    while (sourceIt.hasNext()) {
		String sourceName = sourceIt.next();
		System.out.println(module.name + " [" + module.parallelismHint + "] < " + sourceName);
		if (this.batch) {
		    boltDecl.localOrShuffleGrouping(sourceName/*, new Fields("doc_id")*/);
		} else {
		    if (sourceModules.size() == 1) {
			boltDecl.shuffleGrouping(sourceName);
		    } else { // sourceModules.size() > 1
			boltDecl.fieldsGrouping(sourceName, new Fields("doc_id", "granularity_info"));
		    }
		}
	    }
	    moduleGrns.put(module.name, module.granularity);
	}

	Iterator<OutputModule> outputIt = this.outputModules.iterator();
	while (outputIt.hasNext()) {
	    OutputModule outputModule = outputIt.next();
	    OutputManagerBolt bolt = new OutputManagerBolt(outputModule.name, outputModule.instance, config);
	    BoltDeclarer boltDecl = builder.setBolt(outputModule.name, bolt, outputModule.parallelismHint);
	    if (this.batch) {
		boltDecl.localOrShuffleGrouping(lastNLPBoltName/*, new Fields("doc_id")*/);
	    } else {
		boltDecl.shuffleGrouping(lastNLPBoltName);
	    }
	}
	

	return builder.createTopology();
    }

    public String toString() {
	StringBuilder str = new StringBuilder();
	str.append("==================================================\n")
	    .append("TOPOLOGY: " + this.name + "\n")
	    .append("MODE: " + (this.batch ? "Batch" : "Streaming") + "\n");
	str.append("SPOUTS: \n");
	for (InputModule mod : this.inputModules) {
	    str.append("\t" + mod.name + "\t[" + mod.parallelismHint + "]");
	}
	str.append("BOLTS: \n");
	for (Module mod : this.modules) {
	    str.append("\t" + mod.name + " (src:");
	    for (String src : mod.sourceModules) {
		str.append(" [" + src + "]");
	    }
	    str.append(")" + "\t[" + mod.parallelismHint + "]");
	}
	for (OutputModule mod : this.outputModules) {
	    str.append("\t" + mod.name + "\t[" + mod.parallelismHint + "]");
	}
	str.append("==================================================\n");
	return str.toString();
    }


    private class Module
    {
	public String name;
	public INLPModule instance;
	public List<String> sourceModules;
	public List<String> inLayers;
	public List<String> outLayers;
	public GranularityType granularity;
	public int parallelismHint;
	public String vmType;
      
	public Module(String name, INLPModule instance, List<String> sourceModules, List<String> inLayers, List<String> outLayers, GranularityType granularity, int parallelismHint, String vmType)
	{
	    this.name = name;
	    this.instance = instance;
	    this.sourceModules = sourceModules;
	    this.inLayers = inLayers;
	    this.outLayers = outLayers;
	    this.granularity = granularity;
	    this.parallelismHint = parallelismHint;
	    this.vmType = vmType;
	}
    }

    private class InputModule
    {
	public String name;
	public IInputManager instance;
	public int parallelismHint;

	public InputModule(String name, IInputManager instance, int parallelismHint)
	{
	    this.name = name;
	    this.instance = instance;
	    this.parallelismHint = parallelismHint;
	}
    }

    private class OutputModule
    {
	public String name;
	public IOutputManager instance;
	public int parallelismHint;

	public OutputModule(String name, IOutputManager instance, int parallelismHint)
	{
	    this.name = name;
	    this.instance = instance;
	    this.parallelismHint = parallelismHint;
	}
    }
}
