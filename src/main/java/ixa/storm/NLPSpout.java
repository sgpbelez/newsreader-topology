package ixa.storm;

import ixa.kaflib.KAFDocument;
import backtype.storm.topology.base.BaseRichSpout;
import backtype.storm.spout.SpoutOutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import java.util.Map;
import java.io.StringReader;
import java.io.Reader;


public class NLPSpout extends BaseRichSpout
{
    public static class InputTuple {
	public String name;
	public String text;
    }

    private IInputManager input;
    private String name;
    private SpoutOutputCollector collector;
    private LogManager logManager;
    private TopologyConf appConfig;
    private MongoNaf mongo;
    private Boolean batch;
    private static final long serialVersionUID = 42L; // Serializable...


    public NLPSpout(String name, IInputManager input, TopologyConf config)
    {
	this.input = input;
	this.name = name;
	this.appConfig = config;
	this.batch = config.batchMode();
    }

    public String getName() {
	return this.name;
    }

    @Override
	public void open(Map conf, TopologyContext context, SpoutOutputCollector collector) {
        this.collector = collector;
	this.input.prepare(this.appConfig);
	// Mongo
	try {
	    if (this.appConfig.useMongoDB()) {
		String mongoHost = this.appConfig.getMongoDBHost();
		String mongoPort = this.appConfig.getMongoDBPort();
		String mongoDb = this.appConfig.getMongoDBName();
		this.mongo = MongoNaf.instance(mongoHost, Integer.parseInt(mongoPort), mongoDb);
	    }
	} catch(Exception e) {
	    e.printStackTrace();
	}
	this.logManager = new LogManager(this.appConfig.logStdOutput(), this.appConfig.logMongoDB(), context.getThisWorkerPort());
	if (this.appConfig.logMongoDB()) {
	    this.logManager.setMongoManager(this.mongo);
	}
    }

    @Override
    public void nextTuple() {

	InputTuple next = this.input.getNextDocument();
	if (next != null) {
	    String content = next.text;
	    Reader stream = new StringReader(content);
	    KAFDocument naf;
	    try {
		naf = KAFDocument.createFromStream(stream);
	    } catch (Exception e) {
		// Received content is not NAF, is only text
		naf = new KAFDocument("en", "v1");
		naf.setRawText(content);
	    }

	    try {
		String docId = next.name + "_" + Helper.getMd5(naf.toString());
		if (this.appConfig.useMongoDB()) {
		    this.mongo.insertNafDocument(docId, naf);
		}
		this.logManager.writeDocStartEntry(docId);
		GranularityInfo grnInfo = new GranularityInfo(GranularityType.DOCUMENT);
		Values outputTuple = new Values(docId, grnInfo);
		if (!this.appConfig.useMongoDB()) {
		    outputTuple.add(naf);
		}
		this.input.init(docId);
		this.collector.emit(outputTuple, docId);
	    }
	    catch(Exception e) {
		e.printStackTrace();
		return;
	    }
	}
    }

    @Override
    public void ack(Object id) {
	String docId = (String) id;
	if (this.appConfig.useMongoDB() && this.appConfig.cleanMongoDB()) {
	    this.mongo.removeDoc(docId);
	}
	try {
	    this.logManager.writeDocAckEntry(docId);
	} catch(Exception e) {
	    System.err.println("MongoDB error. Is MongoDB server running?");
	}
	this.input.ack(docId);
    }

    @Override
    public void fail(Object id) {
	String docId = (String) id;
	if (this.appConfig.useMongoDB()) {
	    this.mongo.removeDoc(docId);
	}
	try {
	    this.logManager.writeDocFailEntry(docId);
	} catch(Exception e) {
	    System.err.println("MongoDB error. Is MongoDB server running?");
	}
	this.input.fail(docId);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer) {
	if (this.appConfig.useMongoDB()) {
	    declarer.declare(new Fields("doc_id", "granularity_info"));
	} else {
	    declarer.declare(new Fields("doc_id", "granularity_info", "naf"));
	}
    }

}
