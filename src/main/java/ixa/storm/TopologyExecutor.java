package ixa.storm;

import backtype.storm.LocalCluster;
import backtype.storm.Config;
import backtype.storm.StormSubmitter;
import backtype.storm.generated.StormTopology;
import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;


public class TopologyExecutor
{
    Topology topology;
    TopologyConf appConfig;
    LocalCluster cluster;


    public TopologyExecutor(Topology topology, TopologyConf appConfig)
    {
	this.topology = topology;
	this.appConfig = appConfig;
    }

    public void runLocal(Integer numCommonWorkers, Integer numDedicatedWorkers)
    {
	Config conf = new Config();
        conf.setDebug(false);
	conf.setMessageTimeoutSecs(this.appConfig.getDocTimeout());
	if (this.appConfig.batchMode()) {
	    conf.setMaxSpoutPending(1);
	} else {
	    conf.setMaxSpoutPending(numCommonWorkers);
	}

	// Temporal
	conf.put("outputDir", "/tmp");
	conf.put("failDir", "/tmp");

	String topologyName = this.topology.getName();
	StormTopology stormTopology = this.topology.getStormTopology(this.appConfig, numCommonWorkers + numDedicatedWorkers);
	this.cluster = new LocalCluster();
        this.cluster.submitTopology(topologyName, conf, stormTopology);
    }

    public void run(Integer numCommonWorkers, Integer numDedicatedWorkers) throws AlreadyAliveException, InvalidTopologyException
    {
	Config conf = new Config();
        conf.setDebug(false);
	conf.setMessageTimeoutSecs(this.appConfig.getDocTimeout());
	if (this.appConfig.batchMode()) {
	    conf.setMaxSpoutPending(1);
	} else {
	    conf.setMaxSpoutPending(numCommonWorkers);
	}

	conf.setNumWorkers(numCommonWorkers + numDedicatedWorkers);
	String topologyName = this.topology.getName();
	StormTopology stormTopology = this.topology.getStormTopology(this.appConfig, numCommonWorkers + numDedicatedWorkers);
	StormSubmitter.submitTopology(topologyName, conf, stormTopology);
    }

    public void kill()
    {
	this.cluster.shutdown();
    }

}
