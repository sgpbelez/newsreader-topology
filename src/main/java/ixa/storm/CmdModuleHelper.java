package ixa.storm;

import ixa.kaflib.KAFDocument;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.BufferedWriter;
import java.io.OutputStreamWriter;
import java.util.concurrent.TimeoutException;


public class CmdModuleHelper
{
    private String exePath;


    public CmdModuleHelper(String exePath)
    {
	this.exePath = exePath;
    }

    public KAFDocument run(KAFDocument inputNaf, Integer timeout) throws TimeoutException, Exception
    {
	String[] cmd = {"bash", this.exePath};
	KAFDocument outputNaf = null;

	Process process = null;
	BufferedReader reader = null;
	BufferedReader errorReader = null;
	BufferedWriter writer = null;
	try {
	    Long startMilis = System.currentTimeMillis();
	    Long endMilis = startMilis + (new Long(timeout) * 1000L);
	    ProcessBuilder pb = new ProcessBuilder(cmd);
	    process = pb.start();
	    reader = new BufferedReader(new InputStreamReader(process.getInputStream()));
	    errorReader = new BufferedReader(new InputStreamReader(process.getErrorStream()));
	    writer = new BufferedWriter(new OutputStreamWriter(process.getOutputStream()));
	    writer.write(inputNaf.toString());
	    writer.close();
	    // Wait for results (check timeout and errors)
	    StringBuffer errorBuffer = new StringBuffer();
	    while (!reader.ready() && !processTerminated(process)) {
		if (System.currentTimeMillis() >= endMilis) {
		    if (!errorBuffer.toString().isEmpty()) {
			System.err.println(errorBuffer.toString());
		    }	    
		    throw new TimeoutException();
		}
		while (errorReader.ready()) {
		    String errorLine = errorReader.readLine();
		    errorBuffer.append(errorLine);
		    errorBuffer.append('\n');
		}
	    }
	    // Read standard error output
	    while (errorReader.ready()) {
		String errorLine = errorReader.readLine();
		errorBuffer.append(errorLine);
		errorBuffer.append('\n');
	    }
	    // Check for results (standard output)
	    if (reader.ready()) {
		outputNaf = KAFDocument.createFromStream(reader);
	    } else { // Process terminated without output
		if (!errorBuffer.toString().isEmpty()) {
		    System.err.println(errorBuffer.toString());
		}
		throw new Exception("Module terminated with no output");
	    }
	} catch (TimeoutException e) {
	    throw e;
	} catch(Exception e) {
	    throw e;
	} finally {
	    if (writer != null) {
		writer.close();
	    }
	    if (reader != null) {
		reader.close();
	    }
	    if (errorReader != null) {
		errorReader.close();
	    }
	    if (process != null) {
		process.destroy();
	    }
	}
	return outputNaf;
    }

    private static boolean processTerminated(Process process)
    {
	try {
	    process.exitValue();
	    return true;
	} catch (IllegalThreadStateException e) {
	    return false;
	}
    }
}
