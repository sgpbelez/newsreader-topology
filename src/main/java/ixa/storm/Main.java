package ixa.storm;

import backtype.storm.generated.AlreadyAliveException;
import backtype.storm.generated.InvalidTopologyException;
import org.jdom2.JDOMException;
import java.io.IOException;


public class Main
{
    public static void main(String[] args)
    {
	if (args.length < 4) {
	    System.err.println("Arguments missing");
	    return;
	}

	/* Input arguments */
	String confFile = args[0];
	String componentSpecsFile = args[1];
	Integer numCommonWorkers = Integer.parseInt(args[2]);
	Integer numDedicatedWorkers = Integer.parseInt(args[3]);
	boolean runLocal = true;
	if (args.length > 4) {
	    runLocal = false;
	}
	
	/* Load topology configuration from external file */
	TopologyConf config = new TopologyConf(confFile);
	Topology topology = new Topology("newsreader-pipeline", config.batchMode());

	/* Builds the topology taking into account the number of
	 * machines and the processing mode (batch VS streaming) */
	TopologyBuilder builder = new TopologyBuilder(numCommonWorkers, numDedicatedWorkers, config);
	try {
	    builder.loadTopologyModules(topology, componentSpecsFile);
	} catch (IOException e) {
	    System.err.println("component_specs.xml not found in ~/opt/etc");
	    return;
	} catch (JDOMException e) {
	    System.err.println("Could not parse component_specs.xml");
	    return;
	} catch (IllegalArgumentException e) {
	    System.err.println(e.getMessage());
	    return;
	}

	/* Create a TopologyExecutor object. It will deploy the
	 * topology to the cluster. */
	TopologyExecutor executor = new TopologyExecutor(topology, config);
	
	if (config.debug()) {
	    System.out.println(topology);
	}

	if (runLocal) {
	    executor.runLocal(numCommonWorkers, numDedicatedWorkers);
	} else {
	    try {
		executor.run(numCommonWorkers, numDedicatedWorkers);
	    } catch (AlreadyAliveException e) {
		System.err.println(e.getMessage());
		return;
	    } catch (InvalidTopologyException e) {
		System.err.println(e.getMessage());
		return;
	    }
	}
    }

}
