package ixa.storm;

import ixa.kaflib.KAFDocument;
import java.util.concurrent.TimeoutException;


public interface INLPModule
{
    /* Prepare the module loading all the resources (models...) needed to execute */
    void prepare(TopologyConf config);

    /* Run the module. Returns the updated NAF document. */
    KAFDocument run(KAFDocument naf) throws TimeoutException, Exception;
}
