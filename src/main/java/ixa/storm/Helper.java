package ixa.storm;

import java.security.MessageDigest;


public abstract class Helper {

    public static String getMd5(String str) {
	String digest = null;
        try {
            MessageDigest md = MessageDigest.getInstance("MD5");
	    byte[] hash = md.digest(str.getBytes("UTF-8"));                   
	    StringBuilder sb = new StringBuilder(2*hash.length);
	    for(byte b : hash){
		sb.append(String.format("%02x", b&0xff));
	    }
	    digest = sb.toString();
	} catch (Exception x) {
	    System.err.println(x);
	}
        return digest;
    }

}
