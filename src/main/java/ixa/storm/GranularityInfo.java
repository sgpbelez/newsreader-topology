package ixa.storm;

import java.io.Serializable;


class GranularityInfo implements Serializable
{
    public GranularityType granularityType;
    public Integer paragraphNum;
    public Integer totalParagraphs;
    public Integer sentenceNum;
    public Integer paragraphSentences;
    public Integer totalSentences;

    GranularityInfo(GranularityType type)
    {
	this.granularityType = type;
    }

    GranularityInfo(GranularityType type, Integer parNum, Integer totalPars)
    {
	this(type);
	this.paragraphNum = parNum;
	this.totalParagraphs = totalPars;
    }

    GranularityInfo(GranularityType type, Integer parNum, Integer totalPars, Integer sentNum, Integer parSents, Integer totalSents)
    {
	this(type, parNum, totalPars);
	this.sentenceNum = sentNum;
	this.paragraphSentences = parSents;
	this.totalSentences = totalSents;
    }

    GranularityInfo(GranularityInfo grnInfo)
    {
	this(grnInfo.granularityType, grnInfo.paragraphNum, grnInfo.totalParagraphs,
			grnInfo.sentenceNum, grnInfo.paragraphSentences, grnInfo.totalSentences);
    }

    public Integer getNumberOfParts(GranularityType what, GranularityType where)
    {
	if (what == GranularityType.DOCUMENT) {
	    return 1;
	} else if (what == GranularityType.PARAGRAPH) {
	    return this.totalParagraphs;
	} else if (what == GranularityType.SENTENCE) {
	    if (where == GranularityType.DOCUMENT) {
		return this.totalSentences;
	    } else {
		return this.paragraphSentences;
	    }
	}
	return null;
    }
    
}
