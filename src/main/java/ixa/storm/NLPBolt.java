package ixa.storm;

import ixa.kaflib.KAFDocument;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.concurrent.TimeoutException;
import java.util.Date;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.net.InetAddress;

import java.util.Random;

class NLPBolt extends BaseRichBolt
{
    private String name;
    private INLPModule module;
    private List<String> inLayers;
    private List<String> outLayers;
    private LogManager logManager;
    private OutputCollector collector;
    private TopologyConf appConfig;
    private boolean lastBolt;
    private MongoNaf mongo;
    private Integer workerId;
    private static final long serialVersionUID = 42L; // Serializable...
    
    private Integer testKont;
    private Integer testId;


    NLPBolt(String name, INLPModule module, List<String> inLayers, List<String> outLayers, TopologyConf config, boolean lastBolt)
    {
	this.name = name;
	this.module = module;
	this.inLayers = inLayers;
	this.inLayers.addAll(outLayers);
	this.outLayers = outLayers;
	this.appConfig = config;
	this.lastBolt = lastBolt;
    }

    @Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector)
    {
	this.collector = collector;
	this.module.prepare(this.appConfig);
	this.workerId = context.getThisWorkerPort();
	// Mongo
	try {
	    if (this.appConfig.useMongoDB()) {
		String mongoHost = this.appConfig.getMongoDBHost();
		String mongoPort = this.appConfig.getMongoDBPort();
		String mongoDb = this.appConfig.getMongoDBName();
		this.mongo = MongoNaf.instance(mongoHost, Integer.parseInt(mongoPort), mongoDb);
	    }
	} catch(Exception e) {
	    e.printStackTrace();
	}
	this.logManager = new LogManager(this.appConfig.logStdOutput(), this.appConfig.logMongoDB(), this.workerId);
	if (this.appConfig.logMongoDB()) {
	    this.logManager.setMongoManager(this.mongo);
	}
		
	this.testKont = 0;
	Random rnd = new Random();
	this.testId = rnd.nextInt();
    }

    @Override
    public void execute(Tuple tuple)
    {
	// System.out.println("+++++ RECEIVE | KONT: " + this.testKont + " (" + this.testId + ")");
	this.testKont += 1;
	// System.out.println(this.testId + " kont inkrementatu da " + this.testKont + " izatera.");
	
	String docId = tuple.getStringByField("doc_id");
	GranularityInfo grnInfo = (GranularityInfo) tuple.getValueByField("granularity_info");
        KAFDocument naf = null;

	try {
	    this.logManager.writeBoltRcvEntry(docId, this.name);
	} catch (Exception e) {}

	if (this.appConfig.useMongoDB()) {
	    // Get current NAF document from MongoDB
	    try {
		naf = mongo.getNaf(docId, this.inLayers, grnInfo.paragraphNum, grnInfo.sentenceNum);   
	    } catch (Exception e) {
		System.err.println("MongoNaf: Error loading NAF from MongoDB:");
		e.printStackTrace();
		this.failTuple(tuple);
		return;
	    }
	} else {
	    naf = (KAFDocument) tuple.getValueByField("naf");
	}

	if (this.appConfig.debug()) {
	    try {
		this.debugBeforeExecuting(docId, naf);
	    } catch (Exception e) {
		System.err.println(">>> An error occurred when deserialising the NAF document:");
		e.printStackTrace();
		System.err.println("<<< [END-KAFLIB-ERROR]");
	    }
	}

	Date startTimestamp = new Date();
	try {
	    naf = this.module.run(naf);
	} catch (TimeoutException e) {
	    System.err.println("Timeout: " + this.name);
	    this.failTuple(tuple);
	    return;
	} catch(Exception e) {
	    e.printStackTrace();
	    this.failTuple(tuple);
	    return;
	}
	Date endTimestamp = new Date();

	if (this.appConfig.debug()) {
	    try {
		this.debugAfterExecuting(docId, naf);
	    } catch (Exception e) {
		System.err.println(">>> An error occurred when serialising the NAF document:");
		e.printStackTrace();
		System.err.println("<<< [END-KAFLIB-ERROR]");
	    }
	}

	// Overwrite LP info in NAF
	this.overwriteHeader(naf, startTimestamp, endTimestamp);

	Values outputTuple = new Values(docId, grnInfo);
	if (this.appConfig.useMongoDB()) {
	    try {
		// Store the header info
		mongo.insertLinguisticProcessors(docId, naf);
		// Store the result in MongoDB
		for (String outLayer : this.outLayers) {
		    if (!outLayer.equals("all")) {
			mongo.insertLayer(docId, naf, outLayer, grnInfo.paragraphNum, grnInfo.sentenceNum);
		    } else {
			mongo.insertNafDocument(docId, naf, grnInfo.paragraphNum, grnInfo.sentenceNum); // Insert full NAF
		    }
		}
	    } catch (Exception e) {
		System.err.println("MongoNaf: Error writing NAF to MongoDB:");
		e.printStackTrace();
		this.failTuple(tuple);
		return;
	    }
	} else {
	    outputTuple.add(naf);
	}
	try {
	    this.logManager.writeBoltEmtEntry(docId, this.name);
	} catch(Exception e) {}
	if (!this.lastBolt) {
	    this.collector.emit(tuple, outputTuple);
	}
	this.collector.ack(tuple);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer)
    {
	if (this.appConfig.useMongoDB()) {
	    declarer.declare(new Fields("doc_id", "granularity_info"));
	} else {
	    declarer.declare(new Fields("doc_id", "granularity_info", "naf"));
	}
    }

    private void failTuple(Tuple tuple) {
	try {
	    this.logManager.writeBoltFailEntry(tuple.getStringByField("doc_id"), this.name);
	} catch(Exception e) {
	    System.err.println("MongoDB error. Is MongoDB server running?");
	}
	this.collector.fail(tuple);
    }


    private void debugBeforeExecuting(String docId, KAFDocument naf) throws Exception {
	String layers = "";
	for (String layer : this.inLayers) {
	    layers += layer + " | ";
	}
	System.out.println("-----------------------------------------");
	System.out.println("Debug info before processing...");
	System.out.println("Module: " + this.name);
	System.out.println("Document: " + docId);
	System.out.println("Layers to read: " + layers);
	System.out.println("NAF document read: ");
	System.out.println(naf);
	System.out.println("-----------------------------------------");
    }

    private void debugAfterExecuting(String docId, KAFDocument naf) throws Exception {
	String layers = "";
	for (String layer : this.outLayers) {
	    layers += layer + " | ";
	}
	System.out.println("-----------------------------------------");
	System.out.println("Debug info after processing...");
	System.out.println("Module: " + this.name);
	System.out.println("Document: " + docId);
	System.out.println("Layers to write: " + layers);
	System.out.println("NAF document to write: ");
	System.out.println(naf);
	System.out.println("-----------------------------------------");
    }

    private void overwriteHeader(KAFDocument naf, Date start, Date end) {
	DateFormat df = new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ssZ");
	String hostname;
	try {
	    hostname = InetAddress.getLocalHost().getHostName();
	} catch (Exception e) {
	    hostname = "unknown";
	}
	String host = hostname + "_" + Integer.toString(this.workerId);
	for (KAFDocument.LinguisticProcessor lp : naf.getLinguisticProcessorList()) {
	    lp.setBeginTimestamp(df.format(start));
	    lp.setEndTimestamp(df.format(end));
	    lp.setHostname(host);
	}
    }

}
