package ixa.storm;

import ixa.kaflib.KAFDocument;
import java.io.Serializable;
import java.util.concurrent.TimeoutException;


public class NLPModule implements INLPModule, Serializable
{
    String execPath;
    String lang;
    Integer timeout;
    private static final long serialVersionUID = 42L; // Serializable...

    public NLPModule(String execPath, String lang, Integer timeout)
    {
	this.execPath = execPath;
	this.lang = lang;
	this.timeout = timeout;
    }

    public void prepare(TopologyConf config)
    {}

    public KAFDocument run(KAFDocument naf) throws TimeoutException, Exception
    {
	CmdModuleHelper helper = new CmdModuleHelper(this.execPath);
	KAFDocument newNaf = helper.run(naf, this.timeout);
	return newNaf;	
    }
}
