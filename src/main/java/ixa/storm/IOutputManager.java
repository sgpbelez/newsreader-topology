package ixa.storm;

import ixa.kaflib.KAFDocument;


public interface IOutputManager
{
    /* Prepare the module loading all the resources needed to execute */
    void prepare(TopologyConf config);

    /* Run the module. */
    void run(KAFDocument naf, String docId) throws Exception;
}
