package ixa.storm;

import java.util.Date;
import java.net.InetAddress;


class LogManager
{
    private static final String DOC_START_TAG = "DOC_START";
    private static final String ACK_TAG = "DOC_ACK";
    private static final String FAIL_TAG = "DOC_FAIL";
    private static final String BOLT_RCV_TAG = "DOC_BOLT_RCV";
    private static final String BOLT_EMT_TAG = "DOC_BOLT_EMT";
    private static final String BOLT_FAIL_TAG = "DOC_BOLT_FAIL";

    private Boolean writeStdOutput;
    private Boolean writeMongodb;
    private String workerId;
    private MongoNaf mongo;


    LogManager(Boolean writeStdOutput, Boolean writeMongodb, Integer workerPort)
    {
	this.writeStdOutput = writeStdOutput;
	this.writeMongodb = writeMongodb;
	this.workerId = Integer.toString(workerPort);
    }

    void setMongoManager(MongoNaf mongo) {
	this.mongo = mongo;
    }

    void writeDocStartEntry(String docId) throws Exception
    {
	if (this.writeStdOutput) {
	    String text = "[" + DOC_START_TAG + "] " + docId + " " + getTimestamp() + " " + this.getHostname();
	    writeEntryToStdOutput(text);
	}
	if (this.writeMongodb) {
	    this.checkMongoManager();
	    this.mongo.writeDocLogEntry(DOC_START_TAG, docId, getTimestamp(), this.getHostname());
	}
    }

    void writeDocAckEntry(String docId) throws Exception {
	if (this.writeStdOutput) {
	    String text = "[" + ACK_TAG + "] " + docId + " " + getTimestamp() + " " + this.getHostname();
	    writeEntryToStdOutput(text);
	}
	if (this.writeMongodb) {
	    this.checkMongoManager();
	    this.mongo.writeDocLogEntry(ACK_TAG, docId, getTimestamp(), this.getHostname());
	}
    }

    void writeDocFailEntry(String docId) throws Exception {
	if (this.writeStdOutput) {
	    String text = "[" + FAIL_TAG + "] " + docId + " " + getTimestamp() + " " + this.getHostname();
	    writeEntryToStdOutput(text);
	}
	if (this.writeMongodb) {
	    this.checkMongoManager();
	    this.mongo.writeDocLogEntry(FAIL_TAG, docId, getTimestamp(), this.getHostname());
	}
    }

    void writeBoltRcvEntry(String docId, String boltId)	throws Exception {
	if (this.writeStdOutput) {
	    String text = "[" + BOLT_RCV_TAG + "] " + docId + " " + boltId + " " + getTimestamp() + " " + this.getHostname();
	    writeEntryToStdOutput(text);
	}
	if (this.writeMongodb) {
	    this.checkMongoManager();
	    this.mongo.writeModuleLogEntry(BOLT_RCV_TAG, docId, boltId, getTimestamp(), this.getHostname());
	}
    }

    void writeBoltEmtEntry(String docId, String boltId)	throws Exception {
	if (this.writeStdOutput) {
	    String text = "[" + BOLT_EMT_TAG + "] " + docId + " " + boltId + " " + getTimestamp() + " " + this.getHostname();
	    writeEntryToStdOutput(text);
	}
	if (this.writeMongodb) {
	    this.checkMongoManager();
	    this.mongo.writeModuleLogEntry(BOLT_EMT_TAG, docId, boltId, getTimestamp(), this.getHostname());
	}
    }

    void writeBoltFailEntry(String docId, String boltId) throws Exception {
	if (this.writeStdOutput) {
	    String text = "[" + BOLT_FAIL_TAG + "] " +  docId + " " + boltId + " " + getTimestamp() + " " + this.getHostname();
	    writeEntryToStdOutput(text);
	}
	if (this.writeMongodb) {
	    this.checkMongoManager();
	    this.mongo.writeModuleLogEntry(BOLT_FAIL_TAG, docId, boltId, getTimestamp(), this.getHostname());
	}
    }

    private static void writeEntryToStdOutput(String entry) {
	System.out.println("LOG >>> " + entry);
    }

    private void checkMongoManager() throws Exception {
	if (this.mongo == null) {
	    System.err.println("You need to set a MongoNaf to the LogManager in order to write log to MongoDB.");
	    throw new Exception("You need to set a MongoNafManager to the LogManager in order to write log to MongoDB.");
	}
    }


    private static Date getTimestamp()
    {
	return new Date();
    }

    private String getHostname()
    {
	try {
	    return InetAddress.getLocalHost().getHostName() + "_" + this.workerId;
	} catch(Exception e) {
	    return "unknown";
	}
    }

}
