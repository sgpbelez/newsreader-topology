package ixa.storm;

import ixa.storm.IOutputManager;
import ixa.storm.TopologyConf;
import ixa.storm.Helper;
import ixa.kaflib.KAFDocument;

import java.io.Serializable;
import java.util.Properties;

import kafka.javaapi.producer.Producer;
import kafka.producer.KeyedMessage;
import kafka.producer.ProducerConfig;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.OutputStream;

import org.apache.commons.compress.compressors.bzip2.BZip2CompressorOutputStream;

import com.rabbitmq.client.Channel;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.ConnectionFactory;

import ixa.kafka.KafkaDoc;

public class RabbitMQOutputModule implements IOutputManager, Serializable
{
    private static final String TOPIC = "output_queue";
    private String HOST;
    private String PORT;
    private static final long serialVersionUID = 42L; // Serializable...

    public void prepare(TopologyConf config)
    {
		this.HOST = config.getKafkaHost();
		this.PORT= config.getKafkaPort();
    }

    public void run(KAFDocument naf, String docId) throws Exception
    {
		String nafContent = naf.toString();

		// MD5 of the resulting NAF document
		String newMd5 = Helper.getMd5(nafContent);

		// Prepare the output document's name
		String outputDocId = RabbitMQOutputModule.prepareDocName(docId, newMd5);

		ByteArrayOutputStream contentOs = new ByteArrayOutputStream();
		OutputStream bzip2Stream = new BZip2CompressorOutputStream(contentOs);
		bzip2Stream.write(nafContent.getBytes("UTF-8"));
		bzip2Stream.close();
		byte[] compressedContent = contentOs.toByteArray();
		contentOs.close();

		//System.out.println("Connecting to Kafka ("+HOST+":"+PORT+") ...");
		/*
		Properties properties = new Properties();
		properties.put("metadata.broker.list", this.kafkaBrokerHost+":"+this.kafkaBrokerPort);
		//properties.put("broker.list", HOST+":"+Integer.toString(PORT));
		//properties.put("serializer.class", "kafka.serializer.StringEncoder");
		properties.put("serializer.class", "kafka.serializer.DefaultEncoder");
		properties.put("request.required.acks", "1");
		properties.put("producer.type", "sync");
		ProducerConfig config = new ProducerConfig(properties);
		*/
		
		ConnectionFactory factory = new ConnectionFactory();
		factory.setHost(HOST);
		Connection connection = factory.newConnection();
		Channel channel = connection.createChannel();
		channel.queueDeclare(this.TOPIC, false, false, false, null);
		
		try {
			KafkaDoc kdoc = KafkaDoc.createFromContent(outputDocId, compressedContent);
			byte[] msg = kdoc.toMessage();
			channel.basicPublish("", this.TOPIC, null, msg);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		channel.close();
		connection.close();
	}

    private static String prepareDocName(String docId, String md5)
    {
	String md5Removed = docId.substring(0, docId.lastIndexOf("_"));
	String extension = md5Removed.substring(docId.lastIndexOf("."));
	if (!extension.toLowerCase().equals(".xml") && !extension.toLowerCase().equals(".naf")) {
	    extension = "";
	}
	String noExtension = md5Removed.substring(0, md5Removed.lastIndexOf(extension));
	
	return noExtension + "_" + md5 + extension + ".bz2";
    }
}
