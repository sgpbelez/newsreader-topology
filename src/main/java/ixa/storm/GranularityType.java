package ixa.storm;

public enum GranularityType {SENTENCE, PARAGRAPH, DOCUMENT};
