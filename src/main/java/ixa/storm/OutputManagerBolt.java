package ixa.storm;

import ixa.kaflib.KAFDocument;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Fields;
import java.util.Map;


class OutputManagerBolt extends BaseRichBolt
{
    private String name;
    private IOutputManager output;
    private LogManager logManager;
    private OutputCollector collector;
    private TopologyConf appConfig;
    private MongoNaf mongo;
    private static final long serialVersionUID = 42L; // Serializable...


    OutputManagerBolt(String name, IOutputManager output, TopologyConf config)
    {
	this.name = name;
	this.output = output;
	this.appConfig = config;
    }

    @Override
    public void prepare(Map conf, TopologyContext context, OutputCollector collector)
    {
	this.collector = collector;
	this.output.prepare(this.appConfig);
	// Mongo
	try {
	    if (this.appConfig.useMongoDB()) {
		String mongoHost = this.appConfig.getMongoDBHost();
		String mongoPort = this.appConfig.getMongoDBPort();
		String mongoDb = this.appConfig.getMongoDBName();
		this.mongo = MongoNaf.instance(mongoHost, Integer.parseInt(mongoPort), mongoDb);
	    }
	} catch(Exception e) {
	    e.printStackTrace();
	}
	this.logManager = new LogManager(this.appConfig.logStdOutput(), this.appConfig.logMongoDB(), context.getThisWorkerPort());
	if (this.appConfig.logMongoDB()) {
	    this.logManager.setMongoManager(this.mongo);
	}
    }

    @Override
    public void execute(Tuple tuple)
    {
	String docId = tuple.getStringByField("doc_id");
        KAFDocument naf = null;
	if (this.appConfig.useMongoDB()) {
	    // Get current NAF document from MongoDB
	    try {
		naf = mongo.getNaf(docId);
	    } catch (Exception e) {
	        e.printStackTrace();
		this.collector.fail(tuple);
		return;
	    }
	} else {	    
	    naf = (KAFDocument) tuple.getValueByField("naf");
	}

	try {
	    this.logManager.writeBoltRcvEntry(docId, this.name);
	    // Run module
	    this.output.run(naf, docId);
	} catch(Exception e) {
	    System.err.println(e);
	    this.collector.fail(tuple);
	    return;
	}

	try {
	    this.logManager.writeBoltEmtEntry(docId, this.name);
	} catch(Exception e)
	    {}
	this.collector.ack(tuple);
    }

    @Override
    public void declareOutputFields(OutputFieldsDeclarer declarer)
    {
	if (this.appConfig.useMongoDB()) {
	    declarer.declare(new Fields("doc_id", "msg"));
	} else {
	    declarer.declare(new Fields("doc_id", "msg", "naf"));
	}
    }
}
