package ixa.storm;

import ixa.storm.NLPSpout.InputTuple;
import ixa.storm.IInputManager;
import ixa.storm.TopologyConf;

import com.rabbitmq.client.ConnectionFactory;
import com.rabbitmq.client.Connection;
import com.rabbitmq.client.Channel;
import com.rabbitmq.client.QueueingConsumer;

import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;
import java.io.Serializable;
import java.io.IOException;
import java.io.FileWriter;
import java.util.Calendar;

import ixa.kafka.KafkaDoc;

public class RabbitMQInputModule implements IInputManager, Serializable
{
    private Channel channel;
    private QueueingConsumer consumer;
    private FileWriter logFile;
    private static final long serialVersionUID = 42L; // Serializable...


    public RabbitMQInputModule()
    {}

    public void prepare(TopologyConf config)
    {
	final String groupId = "default";
	final String topic = "input_queue";
	final String rabbitHost = config.getZKHost();
	/*
	final String zookeeperHost = config.getZKHost();
	final String zookeeperPort = config.getZKPort();
*/
	
	// Configuration
	/*
		Properties properties = new Properties();
		properties.put("zookeeper.connect", zookeeperHost + ":" + zookeeperPort);
		properties.put("group.id", groupId);
        properties.put("auto.commit.interval.ms", "1000");
		properties.put("consumer.timeout.ms", "1000");
		ConsumerConfig kafkaConf = new ConsumerConfig(properties);
	 */

	// Prepare RabbitMQ consumer
	try {
	    ConnectionFactory factory = new ConnectionFactory();
	    factory.setHost(rabbitHost);
	    Connection connection = factory.newConnection();
	    this.channel = connection.createChannel();
	    this.channel.queueDeclare(topic, false, false, false, null);
	    this.consumer = new QueueingConsumer(this.channel);
	    this.channel.basicQos(1); // Send only one document to each worker at a time
	    this.channel.basicConsume(topic, false, this.consumer);
	} catch (Exception e) {
	    e.printStackTrace();
	}

	// Open log file
	try {
	    this.logFile = new FileWriter(config.getLogFile(), true);
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }

    public InputTuple getNextDocument()
    {
	try {
	    QueueingConsumer.Delivery delivery = this.consumer.nextDelivery(1000);
	    if (delivery == null) return null;
	    List<Object> tuple = this.deserialize(delivery.getBody());
	    InputTuple inputTuple = new InputTuple();
	    inputTuple.name = (String) tuple.get(0);
	    inputTuple.text = (String) tuple.get(1);
	    this.channel.basicAck(delivery.getEnvelope().getDeliveryTag(), false);
	    return inputTuple;
	} catch (InterruptedException e) {
	    return null;
	} catch (IOException e) {
	    // Error when sending ACK. Do not do anything, the document will not be removed from the queue.
	    return null;
	}
    }

    public void init(String docId)
    {
	System.out.println("Document " + docId + " has been sent");
    }

    public void ack(String docId) {
	writeLog(this.logFile, "OK: " + docId);
	System.out.println("ACK: " + docId);
    }

    public void fail(String docId) {
	writeLog(this.logFile, "FAIL: " + docId);
	System.out.println("FAIL: " + docId);
    }

    private List<Object> deserialize(byte[] msg) {
	try {
	    KafkaDoc kdoc = KafkaDoc.createFromMessage(msg);
	    String fileName = kdoc.getFilename(null);
	    byte[] content = kdoc.getContent();
	    List<Object> tuple = new ArrayList<Object>();
	    tuple.add(fileName);
	    tuple.add(new String(content, "UTF-8"));
	    return tuple;
	} catch (Exception e) {
	    e.printStackTrace();
	    throw new RuntimeException(e);
	}
    }

    private static void writeLog(FileWriter log, String msg)
    {
	Calendar cal = Calendar.getInstance();
	try {
	    log.write(cal.getTime() + " " + msg + "\n");
	    log.flush();
	} catch (IOException e) {
	    e.printStackTrace();
	}
    }
}
