package ixa.storm;

import ixa.kaflib.KAFDocument;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Comparator;


public class Joiner extends BaseRichBolt
{
    private String name;
    private OutputCollector collector;
    private LogManager logManager;
    private TopologyConf appConfig;
    private GranularityType outputGran;
    private MongoNaf mongo;
    private Integer workerId;
    private Map<String, Integer> docMissingParts;
    private Map<String, List<Tuple>> docParts;
    private static final long serialVersionUID = 42L; // Serializable... 

    
    public Joiner(String name, TopologyConf config, GranularityType outputGran)
    {
	this.name = name;
	this.appConfig = config;
	this.outputGran = outputGran;
	this.docMissingParts = new HashMap<String, Integer>();
	this.docParts = new HashMap<String, List<Tuple>>();
    }

    public void prepare(Map conf, TopologyContext context, OutputCollector collector)
    {
	this.collector = collector;
	this.workerId = context.getThisWorkerPort();
	// Mongo
	try {
	    if (this.appConfig.useMongoDB()) {
		String mongoHost = this.appConfig.getMongoDBHost();
		String mongoPort = this.appConfig.getMongoDBPort();
		String mongoDb = this.appConfig.getMongoDBName();
		this.mongo = MongoNaf.instance(mongoHost, Integer.parseInt(mongoPort), mongoDb);
	    }
	} catch(Exception e) {
	    e.printStackTrace();
	}
	this.logManager = new LogManager(this.appConfig.logStdOutput(), this.appConfig.logMongoDB(), this.workerId);
	if (this.appConfig.logMongoDB()) {
	    this.logManager.setMongoManager(this.mongo);
	}
    }

    public void execute(Tuple tuple)
    {
	String docId = tuple.getStringByField("doc_id");
	GranularityInfo inputGrnInfo = (GranularityInfo) tuple.getValueByField("granularity_info");
	
	try {
	    this.logManager.writeBoltRcvEntry(docId, this.name);
	} catch (Exception e) {}
		
	GranularityInfo outputGrnInfo = new GranularityInfo(this.outputGran);
	if (this.outputGran == GranularityType.PARAGRAPH) {
	    outputGrnInfo.totalParagraphs = inputGrnInfo.totalParagraphs;
	    outputGrnInfo.paragraphNum = inputGrnInfo.paragraphNum;
	}

	Integer totalParts = inputGrnInfo.getNumberOfParts(inputGrnInfo.granularityType, outputGrnInfo.granularityType);
	if (totalParts == 1) {
	    this.process(Arrays.asList(tuple), docId, outputGrnInfo);
	    return;
	}

	Integer missingParts = this.docMissingParts.get(docId);
	if (missingParts == null) {
	    this.docMissingParts.put(docId, totalParts-1);
	    List<Tuple> tuples = new ArrayList<Tuple>();
	    tuples.add(tuple);
	    this.docParts.put(docId, tuples);
	}
	else {
	    missingParts -= 1;
	    List<Tuple> tuples = this.docParts.get(docId);
	    tuples.add(tuple);
	    if (missingParts == 0) {
		this.process(tuples, docId, outputGrnInfo);
	    } else {
		this.docMissingParts.put(docId, missingParts);
		this.docParts.put(docId, tuples);
	    }
	}
    }


    public void declareOutputFields(OutputFieldsDeclarer declarer)
    {
	if (this.appConfig.useMongoDB()) {
	    declarer.declare(new Fields("doc_id", "granularity_info"));
	} else {
	    declarer.declare(new Fields("doc_id", "granularity_info", "naf"));
	}
    }

    private List<KAFDocument> getNafsFromMongo(List<Tuple> tuples) {
	List<KAFDocument> nafs = new ArrayList<KAFDocument>();
	for (Tuple tuple : tuples) {
	    GranularityInfo grnInfo = (GranularityInfo) tuple.getValueByField("granularity_info");
	    Integer paragraph = grnInfo.paragraphNum;
	    Integer sentence = grnInfo.sentenceNum;
	    KAFDocument naf;
	    if (this.appConfig.useMongoDB()) {
		try {
		    naf = (grnInfo.granularityType == GranularityType.PARAGRAPH) ?
			    this.mongo.getNaf(tuple.getStringByField("doc_id"), paragraph, null) :
				this.mongo.getNaf(tuple.getStringByField("doc_id"), paragraph, sentence);
		} catch (Exception e) {
		    e.printStackTrace();
		    this.collector.fail(tuple);
		    return null;
		}
	    } else {
		naf = (KAFDocument) tuple.getValueByField("naf");
	    }
	    nafs.add(naf);
	}
	return nafs;
    }

    private void process(List<Tuple> tuples, String docId, GranularityInfo outputGrnInfo) {
	Integer paragraph = outputGrnInfo.paragraphNum;
	Integer sentence = outputGrnInfo.sentenceNum;
	
	List<KAFDocument> nafs = this.getNafsFromMongo(tuples);
	sort(nafs);
	KAFDocument joinedNaf = KAFDocument.join(nafs);
	
	Values outputTuple = new Values(docId, outputGrnInfo);
	if (this.appConfig.useMongoDB()) {
	    if (paragraph == null) {
		mongo.insertNafDocument(docId, joinedNaf);
	    } else if (sentence == null) {
		mongo.insertNafDocument(docId, joinedNaf, paragraph);
	    } else {
		mongo.insertNafDocument(docId, joinedNaf, paragraph, sentence);
	    }
	} else {
	    outputTuple.add(joinedNaf);
	}
	/* Log */
	try {
	    this.logManager.writeBoltEmtEntry((String)outputTuple.get(0), this.name);
	} catch(Exception e) {}
	this.docMissingParts.remove(docId);
	this.docParts.remove(docId);
	/* Emit */
	this.collector.emit(tuples, outputTuple);
	for (Tuple tuple : tuples) {
	    this.collector.ack(tuple);
	}
    }
    
    private static void sort(List<KAFDocument> nafs) {
	Collections.sort(nafs, new NAFComparator());
    }
    
    private static class NAFComparator implements Comparator<KAFDocument> {
	public int compare(KAFDocument naf1, KAFDocument naf2) {
	    if (naf1.getSentence() == naf2.getSentence()) return 0;
	    else if (naf1.getSentence() < naf2.getSentence()) return -1;
	    else return 1;
	}
    }
    

}