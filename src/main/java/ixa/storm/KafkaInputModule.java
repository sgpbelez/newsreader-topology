package ixa.storm;

import ixa.storm.NLPSpout.InputTuple;
import ixa.storm.IInputManager;
import ixa.storm.TopologyConf;
import kafka.consumer.ConsumerConfig;
import kafka.consumer.KafkaStream;
import kafka.javaapi.consumer.ConsumerConnector;
import kafka.consumer.Consumer;
import kafka.consumer.ConsumerIterator;
import kafka.consumer.ConsumerTimeoutException;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;
import java.util.Properties;
import java.io.Serializable;
import java.io.IOException;
import java.io.FileWriter;
import java.util.Calendar;

import ixa.kafka.KafkaDoc;

public class KafkaInputModule implements IInputManager, Serializable
{
    private ConsumerIterator<byte[], byte[]> iterator;
    private FileWriter logFile;
    private static final long serialVersionUID = 42L; // Serializable...


    public KafkaInputModule()
    {}

    public void prepare(TopologyConf config)
    {
		final String groupId = "default";
		final String topic = "input_queue";
		final String zookeeperHost = config.getZKHost();
		final String zookeeperPort = config.getZKPort();

		// Configuration
		Properties properties = new Properties();
		properties.put("zookeeper.connect", zookeeperHost + ":" + zookeeperPort);
		properties.put("group.id", groupId);
        properties.put("auto.commit.interval.ms", "1000");
		properties.put("consumer.timeout.ms", "1000");
		ConsumerConfig kafkaConf = new ConsumerConfig(properties);

		// Get stream iterator
		final ConsumerConnector consumer = Consumer.createJavaConsumerConnector(kafkaConf);
		Map<String, Integer> topicCountMap = new HashMap<String, Integer>();
        topicCountMap.put(topic, new Integer(1));
		Map<String, List<KafkaStream<byte[], byte[]>>> consumerMap = consumer.createMessageStreams(topicCountMap);
		KafkaStream<byte[], byte[]> stream = consumerMap.get(topic).get(0);
		this.iterator = stream.iterator();

		// Open log file
		try {
			this.logFile = new FileWriter(config.getLogFile(), true);
		} catch (IOException e) {
			e.printStackTrace();
		}
    }

    public InputTuple getNextDocument()
    {
		try {
			if (this.iterator.hasNext()) {
				List<Object> tuple = this.deserialize(this.iterator.next().message());
				InputTuple inputTuple = new InputTuple();
				inputTuple.name = (String) tuple.get(0);
				inputTuple.text = (String) tuple.get(1);
				return inputTuple;
			} else {
				return null;
			}
		} catch (ConsumerTimeoutException e) {
			return null;
		}
    }

    public void init(String docId)
    {
		System.out.println("Document " + docId + " has been sent");
    }

    public void ack(String docId) {
		writeLog(this.logFile, "OK: " + docId);
		System.out.println("ACK: " + docId);
    }

    public void fail(String docId) {
		writeLog(this.logFile, "FAIL: " + docId);
		System.out.println("FAIL: " + docId);
    }

    private List<Object> deserialize(byte[] msg) {
		try {
			KafkaDoc kdoc = KafkaDoc.createFromMessage(msg);
			String fileName = kdoc.getFilename(null);
			byte[] content = kdoc.getContent();
            List<Object> tuple = new ArrayList<Object>();
			tuple.add(fileName);
			tuple.add(new String(content, "UTF-8"));
			return tuple;
        } catch (Exception e) {
			e.printStackTrace();
            throw new RuntimeException(e);
        }
    }

    private static void writeLog(FileWriter log, String msg)
    {
        Calendar cal = Calendar.getInstance();
		try {
			log.write(cal.getTime() + " " + msg + "\n");
			log.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
    }
}
