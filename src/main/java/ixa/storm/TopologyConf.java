package ixa.storm;

import java.util.Properties;
import java.io.Serializable;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileNotFoundException;
import java.io.IOException;

public class TopologyConf implements Serializable
{
    private Properties conf;
    private static final long serialVersionUID = 42L; // Serializable...

    TopologyConf(String configFile) {
	// Default configuration parameters
	this.conf = new Properties();
	this.conf.setProperty("zookeeper.host", "localhost");
	this.conf.setProperty("mongodb.host", "localhost");
	this.conf.setProperty("kafka.host", "localhost");
	this.conf.setProperty("zookeeper.port", "2181");
	this.conf.setProperty("mongodb.port", "27017");
	this.conf.setProperty("kafka.port", "9092");
	this.conf.setProperty("mongodb.dbname", "newsreader-pipeline");
	this.conf.setProperty("mongodb", "true");
	this.conf.setProperty("clean_mongodb", "true");
	this.conf.setProperty("input_dir", "/tmp/input");
	this.conf.setProperty("output_dir", "/tmp/output");
	this.conf.setProperty("log_file", "/tmp/pipeline.log");
	this.conf.setProperty("log_std_output", "false");
	this.conf.setProperty("log_mongodb", "true");
	this.conf.setProperty("doc_timeout", "900"); // 15 min
	this.conf.setProperty("debug", "false");
	this.conf.setProperty("processing_mode", "streaming");
	
	// Overwrite default configuration with user's values
	try {
	    BufferedReader configReader = new BufferedReader(new FileReader(configFile));
	    this.conf.load(configReader);
	} catch (FileNotFoundException e) {
	    System.err.println("No configuration file has been found. Default conf will be used.");
	} catch (IOException e) {
	    System.err.println("There is a syntactic error in the configuration file. Default conf will be used.");
	}
    }

    public String getZKHost() {
	return this.conf.getProperty("zookeeper.host");
    }

    public String getMongoDBHost() {
	return this.conf.getProperty("mongodb.host");
    }

    public String getKafkaHost() {
	return this.conf.getProperty("kafka.host");
    }

    public String getZKPort() {
	return this.conf.getProperty("zookeeper.port");
    }
    
    public String getMongoDBPort() {
	return this.conf.getProperty("mongodb.port");
    }

    public String getKafkaPort() {
	return this.conf.getProperty("kafka.port");
    }

    public String getMongoDBName() {
	return this.conf.getProperty("mongodb.dbname");
    }

    public Boolean useMongoDB() {
	return this.conf.getProperty("mongodb").equals("true");
    }

    public Boolean cleanMongoDB() {
	return this.conf.getProperty("clean_mongodb").equals("true");
    }

    public String getInputDir() {
	return this.conf.getProperty("input_dir");
    }

    public String getOutputDir() {
	return this.conf.getProperty("output_dir");
    }

    public String getLogFile() {
	return this.conf.getProperty("log_file");
    }

    public Boolean logStdOutput() {
	return this.conf.getProperty("log_std_output").equals("true");
    }

    public Boolean logMongoDB() {
	return this.conf.getProperty("log_mongodb").equals("true");
    }

    public Integer getDocTimeout() {
	return Integer.parseInt(this.conf.getProperty("doc_timeout"));
    }

    public Boolean debug() {
	return this.conf.getProperty("debug").equals("true");
    }

    public Boolean batchMode() {
	return this.conf.getProperty("processing_mode").equals("batch");
    }

    public Boolean streamingMode() {
	return !this.conf.getProperty("processing_mode").equals("batch");
    }
}
