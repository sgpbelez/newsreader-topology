package ixa.storm;

import ixa.kaflib.KAFDocument;
import backtype.storm.topology.base.BaseRichBolt;
import backtype.storm.task.OutputCollector;
import backtype.storm.task.TopologyContext;
import backtype.storm.topology.OutputFieldsDeclarer;
import backtype.storm.tuple.Tuple;
import backtype.storm.tuple.Fields;
import backtype.storm.tuple.Values;

import java.util.Arrays;
import java.util.List;
import java.util.Properties;
import java.util.Map;


public class Splitter extends BaseRichBolt
{
    private String name;
    private OutputCollector collector;
    private LogManager logManager;
    private TopologyConf appConfig;
    private GranularityType outputGran;
    private MongoNaf mongo;
    private Integer workerId;
    private static final long serialVersionUID = 42L; // Serializable...    


    public Splitter(String name, TopologyConf config, GranularityType outputGran)
    {
	this.name = name;
	this.appConfig = config;
	this.outputGran = outputGran;
    }

    public void prepare(Map conf, TopologyContext context, OutputCollector collector)
    {
	this.collector = collector;
	this.workerId = context.getThisWorkerPort();
	// Mongo
	try {
	    if (this.appConfig.useMongoDB()) {
		String mongoHost = this.appConfig.getMongoDBHost();
		String mongoPort = this.appConfig.getMongoDBPort();
		String mongoDb = this.appConfig.getMongoDBName();
		this.mongo = MongoNaf.instance(mongoHost, Integer.parseInt(mongoPort), mongoDb);
	    }
	} catch(Exception e) {
	    e.printStackTrace();
	}
	this.logManager = new LogManager(this.appConfig.logStdOutput(), this.appConfig.logMongoDB(), this.workerId);
	if (this.appConfig.logMongoDB()) {
	    this.logManager.setMongoManager(this.mongo);
	}
    }

    public void execute(Tuple tuple)
    {
	String docId = tuple.getStringByField("doc_id");
	GranularityInfo inputGrnInfo = (GranularityInfo) tuple.getValueByField("granularity_info");
	
	try {
	    this.logManager.writeBoltRcvEntry(docId, this.name);
	} catch (Exception e) {}
	
	KAFDocument naf = null;
	if (this.appConfig.useMongoDB()) {
	    try {
		naf = (inputGrnInfo.granularityType == GranularityType.DOCUMENT) ? this.mongo.getNaf(docId) : this.mongo.getNaf(docId, inputGrnInfo.paragraphNum, null);
	    } catch (Exception e) {
		e.printStackTrace();
		this.collector.fail(tuple);
		return;
	    }
	} else {
	    naf = (KAFDocument) tuple.getValueByField("naf");
	}
	
	switch (outputGran)
	    {
	    case DOCUMENT:
		// DOCUMENT => DOCUMENT, PARAGRAPH => DOCUMENT, SENTENCE => DOCUMENT
		// ERROR
		break;
	    case PARAGRAPH:
		// DOCUMENT => PARAGRAPH
		if (inputGrnInfo.granularityType == GranularityType.DOCUMENT) {
		    List<KAFDocument> paraNafs = naf.splitInParagraphs();
		    for (KAFDocument paraNaf : paraNafs) {
			Integer paragraph = paraNaf.getParagraph();
			Integer numParagraphs = paraNafs.size();
			GranularityInfo outputGrnInfo = new GranularityInfo(GranularityType.PARAGRAPH, paragraph, numParagraphs);
			this.process(tuple, docId, paraNaf, outputGrnInfo);
		    }
		}
		// PARAGRAPH => PARAGRAPH
		// SENTENCE => PARAGRAPH
		else {
		    // ERROR
		}
		break;
	    case SENTENCE:
		// DOCUMENT => SENTENCE
		if (inputGrnInfo.granularityType == GranularityType.DOCUMENT) {
		    List<KAFDocument> sentNafs = naf.splitInSentences();
		    for (KAFDocument sentNaf : sentNafs) {
			Integer paragraph = sentNaf.getParagraph();
			Integer sentence = sentNaf.getSentence();
			Integer numParagraphs = naf.getNumParagraphs();
			Integer numSentences = sentNafs.size();
			Integer numSentencesInParagraph = naf.getSentsByParagraph(paragraph).size();
			GranularityInfo outputGrnInfo = new GranularityInfo(GranularityType.SENTENCE, paragraph, numParagraphs, sentence, numSentencesInParagraph, numSentences);
			this.process(tuple, docId, sentNaf, outputGrnInfo);
		    }
		}
		// PARAGRAPH => SENTENCE
		else if (inputGrnInfo.granularityType == GranularityType.PARAGRAPH) {
		    List<KAFDocument> sentNafs = naf.splitInSentences();
		    for (KAFDocument sentNaf : sentNafs) {
			Integer paragraph = inputGrnInfo.paragraphNum;
			Integer sentence = sentNaf.getSentence();
			Integer numParagraphs = inputGrnInfo.totalParagraphs;
			Integer numSentences = sentNafs.size();
			Integer numSentencesInParagraph = naf.getSentsByParagraph(paragraph).size();
			GranularityInfo outputGrnInfo = new GranularityInfo(GranularityType.SENTENCE, paragraph, numParagraphs, sentence, numSentencesInParagraph, numSentences);
			this.process(tuple, docId, sentNaf, outputGrnInfo);
		    }
		}
		// SENTENCE => SENTENCE
		else {
		    // ERROR
		}				
	    }
	this.collector.ack(tuple);
    }

    public void declareOutputFields(OutputFieldsDeclarer declarer)
    {
	if (this.appConfig.useMongoDB()) {
	    declarer.declare(new Fields("doc_id", "granularity_info"));
	} else {
	    declarer.declare(new Fields("doc_id", "granularity_info", "naf"));
	}
    }

    private void process(Tuple tuple, String docId, KAFDocument naf, GranularityInfo grnInfo) {
	Integer paragraph = grnInfo.paragraphNum;
	Integer sentence = grnInfo.sentenceNum;
	
	Values outputTuple = new Values(docId, grnInfo);
	if (this.appConfig.useMongoDB()) {
	    if (paragraph == null) {
		mongo.insertNafDocument(docId, naf);
	    } else if (sentence == null) {
		mongo.insertNafDocument(docId, naf, paragraph);
	    } else {
		mongo.insertNafDocument(docId, naf, paragraph, sentence);
	    }
	} else {
	    outputTuple.add(naf);
	}
	/* Log */
	try {
	    this.logManager.writeBoltEmtEntry((String)outputTuple.get(0), this.name);
	} catch(Exception e) {}
	/* Emit */
	//System.out.println(">>> " + sentence + " / " + paragraph);
	this.collector.emit(tuple, outputTuple);
    }

}