package ixa.storm;

import java.util.Arrays;
import java.util.List;
import java.util.ArrayList;
import java.io.IOException;
import org.jdom2.Document;
import org.jdom2.Element;
import org.jdom2.input.SAXBuilder;
import org.jdom2.JDOMException;


public class TopologyBuilder
{
    private static final String[] VALID_LAYERS = {
	"all",
	"raw",
	"text",
	"terms",
	"entities",
	"deps",
	"constituency",
	"chunks",
	"coreferences",
	"opinions",
	"srl",
	"factualitylayer",
	"timeExpressions",
	"temporalRelations",
	"causalRelations"
    };
    private static final String LANG = "en";
    private static final Integer TIMEOUT_SEC = 3600; // 30 min.
    private static final String COMMON_VM_TYPE = "common";


    String topologyName;
    Integer numCommonWorkers;
    Integer numDedicatedWorkers;
    Boolean batchMode;
    List<Module> modules;

    public TopologyBuilder(Integer numCommonWorkers, Integer numDedicatedWorkers, TopologyConf config) {
	this.numCommonWorkers = numCommonWorkers;
	this.numDedicatedWorkers = numDedicatedWorkers;
	this.batchMode = config.batchMode();
	this.modules = new ArrayList<Module>();
    }

    public void loadTopologyModules(Topology topology, String filepath) throws IOException, JDOMException {
	this.loadTopologyInfo(filepath);

	if (this.topologyName != null) {
	    topology.setName(this.topologyName);
	}

	this.calculateNumExecs();

	/* Input modules (Spouts) */
	Integer numKafkaInputs = this.batchMode ? this.numCommonWorkers : 1;
	topology.setInput("RabbitMQInput", new RabbitMQInputModule(), numKafkaInputs);
	//topology.setInput("KafkaInput", new KafkaInputModule(), numKafkaInputs);
	//topology.setInput("directoryInput", new DirectoryInputModule(), 1);

	/* Output modules */
	Integer numKafkaOutputs = this.batchMode ? this.numCommonWorkers : 1;
	topology.setOutput("RabbitMQOut", new RabbitMQOutputModule(), numKafkaOutputs);
	//topology.setOutput("KafkaOut", new KafkaOutputModule(), numKafkaOutputs);
	//topology.setOutput("xmlOut", new WriterOutputModule(), 1);

	/* Modules */
	for (Module modDef : this.modules) {
	    if (modDef.sources.isEmpty()) {
		modDef.sources.addAll(topology.getInputNames());
	    }
	    Integer parallelismHint;
	    if (this.batchMode) {
		parallelismHint = modDef.toRunOnDedicatedWorker() ? this.numDedicatedWorkers : this.numCommonWorkers;
	    } else {
		parallelismHint = modDef.parallelismHint;
	    }
	    INLPModule module = new NLPModule(modDef.runPath, LANG, TIMEOUT_SEC);
	    topology.setModule(modDef.name, module, parallelismHint, modDef.inLayers, modDef.outLayers, modDef.granularity, modDef.sources, modDef.vmType);
	}
    }

    private void loadTopologyInfo(String filepath)
	throws IOException, JDOMException, IllegalArgumentException {
	SAXBuilder builder = new SAXBuilder();
	String path = "./component_specs.xml";
	Document dom = null;
	try {
	    dom = (Document) builder.build(path);
	} catch (Exception e) {
	    path = filepath;
	    dom = (Document) builder.build(path);
	}

	Element rootElem = dom.getRootElement();

	// Cluster info
	String topologyName = rootElem.getAttributeValue("name");
	if (topologyName != null) {
	    this.topologyName = topologyName;
	}

	String componentsBaseDir = System.getProperty("user.home") + "/components/";
	Element clusterElem = rootElem.getChild("cluster");
	if (clusterElem != null) {
	    String componentsBaseDirAttr = clusterElem.getAttributeValue("componentsBaseDir");
	    if (componentsBaseDirAttr != null) {
		componentsBaseDir = componentsBaseDirAttr;
	    }
	    if (componentsBaseDir.charAt(componentsBaseDir.length() - 1) != '/') {
		componentsBaseDir += "/";
	    }
	}

	// Module info
	List<Element> moduleElems = rootElem.getChildren("module");
	List<String> validModules = new ArrayList<String>();
	String lastModule = null;
	for (Element moduleElem : moduleElems) {

	    String name = moduleElem.getAttributeValue("name");
	    if (name == null) {
		throw new IllegalArgumentException("Error parsing component_specs.xml: 'module:name' must be specified");
	    }

	    String inLayersStr = moduleElem.getAttributeValue("input");
	    if (inLayersStr == null) {
		inLayersStr = "all";
	    }
	    List<String> inLayers = TopologyBuilder.parseLayers(inLayersStr);
	    
	    String outLayersStr = moduleElem.getAttributeValue("output");
	    if (outLayersStr == null) {
		outLayersStr = "all";
	    }
	    List<String> outLayers = TopologyBuilder.parseLayers(outLayersStr);

	    String procTimeStr = moduleElem.getAttributeValue("procTime");
	    Integer procTime = null;
	    if (procTimeStr != null) {
		try {
		    procTime = Integer.parseInt(procTimeStr);
		} catch (NumberFormatException e) {
		    throw new IllegalArgumentException("Error parsing component_specs.xml: 'module:procTime' must be a valid integer");
		}
	    }
	    
	    String numExecStr = moduleElem.getAttributeValue("numExec");
	    Integer numExec = null;
	    if (numExecStr != null) {
		try {
		    numExec = Integer.parseInt(numExecStr);
		} catch (NumberFormatException e) {
		    throw new IllegalArgumentException("Error parsing component_specs.xml: 'module:numExec' must be a valid integer");
		}
	    } else {
		if (procTime == null) {
		    System.out.println("WARN: you didn't define numExec nor procTime for the module " + name + ". No parallelism will be set for that module.");
		}
	    }

	    String runPath = moduleElem.getAttributeValue("runPath");
	    if (runPath == null) {
		throw new IllegalArgumentException("Error parsing component_specs.xml: 'module:runPath' must be specified");
	    }
	    if (runPath.charAt(0) != '/') { // relative path
		runPath = componentsBaseDir + runPath;
	    }

	    String sourcesStr = moduleElem.getAttributeValue("source");
	    List<String> sources = new ArrayList<String>();
	    if (sourcesStr == null) {
		if (lastModule != null) {
		    sources.add(lastModule);
		}
	    } else { // (sources != null)
		sources = TopologyBuilder.parseSources(sourcesStr, validModules);
	    }
	    
	    String granularity = moduleElem.getAttributeValue("granularity");
	    GranularityType grnType = null;
	    if (granularity == null) {
		grnType = GranularityType.DOCUMENT;
	    } else {
		switch (granularity) {
		case "document":
		    grnType = GranularityType.DOCUMENT;
		    break;
		case "paragraph":
		    grnType = GranularityType.PARAGRAPH;
		    break;
		case "sentence":
		    grnType = GranularityType.SENTENCE;
		    break;
		default:
		    grnType = GranularityType.DOCUMENT;
		}
	    }
	    
	    String vmType = moduleElem.getAttributeValue("vm_type");
	    	    
	    Module moduleDef = new Module(name, runPath, inLayers, outLayers, grnType, vmType);
	    moduleDef.procTime = procTime;
	    moduleDef.parallelismHint = numExec;
	    moduleDef.sources = sources;
	    this.modules.add(moduleDef);

	    validModules.add(moduleDef.name);
	    lastModule = moduleDef.name;
	}
    }

    private static List<String> parseLayers(String layersStr) {
	List<String> layers = new ArrayList<String>();
	String[] splitLayers = layersStr.split(",");
	for (String layerStr : splitLayers) {
	    if (TopologyBuilder.isValidLayer(layerStr)) {
		layers.add(layerStr);
	    } else {
		layers.clear();
		layers.add("all");
		return layers;
	    }
	}
	if (layers.isEmpty()) {
	    layers.add("all");
	}
	return layers;
    }

    private static List<String> parseSources(String sourcesStr, List<String> validSources) {
	List<String> sources = new ArrayList<String>();
	String[] splitSources = sourcesStr.split(",");
	for (String source : splitSources) {
	    if (validSources.contains(source)) {
		sources.add(source);
	    } else {
		throw new IllegalArgumentException("Error parsing component_specs.xml: source module '" + source + "' not defined as a module.");
	    }
	}
	return sources;
    }

    private static Boolean isValidLayer(String layer) {
	return Arrays.asList(VALID_LAYERS).contains(layer);
    }

    private void calculateNumExecs() {
	for (Module modDef : this.modules) {
	    if (modDef.parallelismHint == null) {
		if (modDef.procTime == null) {
		    modDef.parallelismHint = 1;
		} else {
		    Integer numWorkers = ((modDef.vmType == null) || modDef.vmType.isEmpty() || modDef.vmType.equals(COMMON_VM_TYPE)) ? this.numCommonWorkers : this.numDedicatedWorkers;
		    modDef.parallelismHint =
			    (((modDef.procTime * numWorkers) -1) / this.getMaxProcTime()) + 1;
		}
	    }
	}
    }

    private Integer getMaxProcTime() {
	Integer max = 0;
	for (Module modDef : this.modules) {
	    if (modDef.procTime != null) {
		max = (modDef.procTime > max) ? modDef.procTime : max;
	    }
	}
	return max;
    }


    private static class Module
    {
	String name;
	String runPath;
	Integer procTime;
	Integer parallelismHint;
	List<String> inLayers;
	List<String> outLayers;
	List<String> sources;
	GranularityType granularity;
	String vmType;

	Module(String name, String runPath, List<String> inLayers, List<String> outLayers, GranularityType granularity, String vmType) {
	    this.name = name;
	    this.runPath = runPath;
	    this.inLayers = inLayers;
	    this.outLayers = outLayers;
	    this.granularity = granularity;
	    this.vmType = vmType;
	}
	
	Boolean toRunOnDedicatedWorker() {
	    return ((this.vmType != null) && !this.vmType.isEmpty() && !this.vmType.equals(COMMON_VM_TYPE));
	}
    }

}
