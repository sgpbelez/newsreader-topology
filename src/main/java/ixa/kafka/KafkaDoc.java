package ixa.kafka;

import java.util.Arrays;
import java.io.IOException;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.nio.file.Path;
import java.nio.file.FileSystems;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.channels.Channels;
import java.nio.channels.ReadableByteChannel;
import java.nio.ByteBuffer;

import joptsimple.*;

// Docs as stored in the Kafka message queue


public class KafkaDoc {

	public static void usage(String str) {

		int status = 0;
		System.out.println("Usage: KafkaDoc [-h] [-n] [-m oDir] [-f input_file ]");
		System.out.println("\t-h\tthis help");
		System.out.println("\t-f input_file\t create a message given an input file");
		System.out.println("\t-m oDir\tgiven a message (from stdin), create a file in directory oDir");
		System.out.println("\t-m oDir\tgiven a message (from stdin), output name and size");
		System.out.println("\t-v\tve berbose");
		System.out.println();
		System.out.println("Tipical usages:");
		System.out.println("\t java -jar ixa.kafka.KafkaDoc -f file > msg.data");
		System.out.println("\t cat msg.data | java -jar ixa.kafka.KafkaDoc -m /tmp");
		System.out.println("\t cat msg.data | java -jar ixa.kafka.KafkaDoc -n");
		if (str != null) {
			System.out.println();
			System.out.println(str);
			System.out.println();
			status = 1;
		}
		System.exit(status);
	}

    public static void main(String[] args) throws Exception {

		String inputFile = null;
		String oDir = null;
		boolean optMsg = false;
		boolean optFile = false;
		boolean optFname = false;

		OptionParser parser = new OptionParser("hf:m:n");
		OptionSet options = null;
		try {
			options = parser.parse(args);
		} catch (Exception e) {
			usage("Error :" + e.getMessage());
			System.exit(1);
		}

		if (options.has("h")) {
			usage(null);
		}

		if (options.has("f")) {
			optFile = true;
			inputFile = (String)options.valueOf("f");
		}

		if (options.has("n")) {
			optFname = true;
		}

		if (options.has("m")) {
			optMsg = true;
			oDir = (String)options.valueOf("m");
		}

		if (!optFile && !optMsg && ! optFname) {
			usage("Please specify one of -f, -m, -n");
		}

		if (optFile) {
			KafkaDoc kdoc = KafkaDoc.createFromFile(inputFile);
			byte [] msg = kdoc.toMessage();
			System.out.write(msg);
		} else {
			// Readg message from STDIN
			ByteBuffer buf = ByteBuffer.allocate(1000000);
			ReadableByteChannel channel = Channels.newChannel(System.in);
			while (channel.read(buf) >= 0)
				;
			buf.flip();
			byte[] bytes = Arrays.copyOf(buf.array(), buf.limit());
			KafkaDoc kdoc =  KafkaDoc.createFromMessage(bytes);
			if (optFname) {
				System.out.println(kdoc.getFilename(null) + " " + kdoc.getSize());
			} else {
				//  and create a file
				if(!Files.exists(Paths.get(oDir))) {
					System.err.println("ERROR: output directory does not exist");
					System.exit(1);
				}
				kdoc.writeFile(oDir);
			}
		}
	}

	String m_name = null;
	// TODO timestamp
	private byte[] m_content = null;

	public static KafkaDoc createFromFile(String fileName) throws IOException {
		return new KafkaDoc(fileName);
	}

	public static KafkaDoc createFromMessage(byte[] message) throws IOException {
		return new KafkaDoc(message);
	}

	public static KafkaDoc createFromContent(String fileName, byte[] content) throws IOException {
		return new KafkaDoc(fileName, content);
	}

	// Given a file name, create a KafkaDoc object
	private KafkaDoc(String argFile) throws IOException {
		String dir = "";
		int idx = argFile.lastIndexOf("/") + 1;
		if (idx != 0) {
			dir = argFile.substring(0, idx);
		}
		m_name = argFile.substring(idx);
		if (m_name.length() == 0) {
			//
		}
		readFile(dir, m_name);
	}

	// Given a kafka message (bytes), create a KafkaDoc object
	private KafkaDoc(byte [] bytes) throws IOException {
		ByteArrayInputStream bs = new ByteArrayInputStream(bytes);
		ObjectInputStream is = new ObjectInputStream(bs);
		try {
			m_name = (String) is.readObject();
		} catch (ClassNotFoundException cnfe) {
			throw new IOException("Bad format");
		}
		Integer contentSize = is.readInt();
		m_content = new byte[contentSize];
		is.readFully(m_content);
		is.close();
	}

	private KafkaDoc (String fileName, byte[] content) {
		m_name = fileName;
		m_content = content;
	}


	private void readFile(String dir, String fileName) throws IOException {
		Path p = null;
		try {
			p = FileSystems.getDefault().getPath(dir, fileName);
			m_content = Files.readAllBytes(p);
		} catch (Exception e) {
			System.err.println("Error: can not read " + p);
			System.exit(1);
		}
	}

	public byte[] toMessage() throws IOException {

		ByteArrayOutputStream bs = new ByteArrayOutputStream();
		ObjectOutputStream os = new ObjectOutputStream(bs);

		os.writeObject(m_name);
		os.writeInt(m_content.length);
		os.write(m_content);
		os.close();
		return bs.toByteArray();
	}

	public void writeFile(String dir) throws Exception {
		Path p = null;
		p = FileSystems.getDefault().getPath(dir, m_name);
		Files.write(p, m_content);
	}

	public String getFilename(String dir) {
		if (dir != null) {
			return dir + "/" + m_name;
		}
		return m_name;
	}

	public byte[] getContent() {
		return m_content;
	}

	public int getSize() {
		return m_content.length;
	}

	public String ls() {
		String res = getFilename(null);
		res += "\t";
		res += m_content.length;
		return res;
	}
};
